# Buttplug API Reference

## Client

| Call        | Parameter types | Returns | Description |
| ----------- | ------ | ------- |----------- |
| `new_insecure(name, addr)`| String, String | [client](#client(object)) | Connects to an external buttplug server using unencrypted websockets. |
| `new_secure(name, addr[, bypass_cert_verify])`| String, String[, bool] | [client](#client(object)) | Connects to an external buttplug server using encrypted websockets. |
| `new_embedded(name)`| String | [client](#client(object)) | Start and connect to the embedded buttplug server. |

### client(object)

### client device(object)

## Server

Not implemented.
