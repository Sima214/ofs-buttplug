use buttplug::{
  client::{
    device::{
      ButtplugClientDevice, ButtplugClientDeviceMessageType, LinearCommand, RotateCommand,
      VibrateCommand,
    },
    ButtplugClient, ButtplugClientError, ButtplugClientEvent,
  },
  connector::{
    ButtplugInProcessClientConnector, ButtplugRemoteClientConnector,
    ButtplugWebsocketClientTransport,
  },
  core::{errors::ButtplugError, messages::serializer::ButtplugClientJSONSerializer},
  device::Endpoint,
};

use futures::StreamExt;
use mlua::{prelude::*, LuaSerdeExt};
use std::collections::HashMap;
use std::convert::TryInto;
use std::sync::Arc;
use tokio::runtime::Runtime;
use tracing_subscriber;

fn buttplug_client_error_to_lua_message(err: ButtplugClientError) -> String {
  match err {
    ButtplugClientError::ButtplugConnectorError(msg) => {
      format!("ConnectorError: {}.", msg)
    }
    ButtplugClientError::ButtplugError(error) => match error {
      ButtplugError::ButtplugHandshakeError(msg) => {
        format!("HandshakeError: {}.", msg)
      }
      ButtplugError::ButtplugDeviceError(msg) => {
        format!("DeviceError: {}.", msg)
      }
      ButtplugError::ButtplugMessageError(msg) => {
        format!("MessageError: {}.", msg)
      }
      ButtplugError::ButtplugPingError(msg) => {
        format!("PingError: {}.", msg)
      }
      ButtplugError::ButtplugUnknownError(msg) => {
        format!("UnknownError: {}.", msg)
      }
    },
  }
}

fn buttplug_error_to_lua_message(err: ButtplugError) -> String {
  match err {
    ButtplugError::ButtplugHandshakeError(msg) => {
      format!("HandshakeError: {}.", msg)
    }
    ButtplugError::ButtplugDeviceError(msg) => {
      format!("DeviceError: {}.", msg)
    }
    ButtplugError::ButtplugMessageError(msg) => {
      format!("MessageError: {}.", msg)
    }
    ButtplugError::ButtplugPingError(msg) => {
      format!("PingError: {}.", msg)
    }
    ButtplugError::ButtplugUnknownError(msg) => {
      format!("UnknownError: {}.", msg)
    }
  }
}

fn lua_vibrate_to_rust(cmd: LuaValue) -> LuaResult<VibrateCommand> {
  match cmd {
    LuaValue::Table(t) => {
      let mut patt_map: HashMap<u32, f64> = HashMap::new();
      for kv in t.pairs::<LuaInteger, LuaNumber>() {
        let (k, v) = kv?;
        match k.try_into() {
          Ok(krl) => {
            patt_map.insert(krl, v);
          }
          Err(err) => {
            return Err(LuaError::MemoryError(format!(
              "Device index out of range! {}",
              err
            )));
          }
        }
      }
      let patt = VibrateCommand::SpeedMap(patt_map);
      Ok(patt)
    }
    LuaValue::Number(v) => {
      let patt = VibrateCommand::Speed(v);
      Ok(patt)
    }
    LuaValue::Integer(v) => {
      let patt = VibrateCommand::Speed(v as f64);
      Ok(patt)
    }
    _ => Err(LuaError::FromLuaConversionError {
      from: cmd.type_name(),
      to: "number|table",
      message: None,
    }),
  }
}

fn lua_linear_to_rust(cmd: LuaValue, exarg: Option<LuaNumber>) -> LuaResult<LinearCommand> {
  match cmd {
    LuaValue::Table(t) => {
      let mut patt_map: HashMap<u32, (u32, f64)> = HashMap::new();
      for kv in t.pairs::<LuaInteger, LuaTable>() {
        let (k, v) = kv?;
        let dur: u32;
        if v.contains_key(1)? {
          dur = v.get(1)?;
        } else if v.contains_key("dur")? {
          dur = v.get("dur")?;
        } else {
          dur = v.get("duration")?;
        }
        let pos: f64;
        if v.contains_key(2)? {
          pos = v.get(2)?;
        } else if v.contains_key("pos")? {
          pos = v.get("pos")?;
        } else {
          pos = v.get("position")?;
        }
        match k.try_into() {
          Ok(krl) => {
            patt_map.insert(krl, (dur, pos));
          }
          Err(err) => {
            return Err(LuaError::MemoryError(format!(
              "Device index out of range! {}",
              err
            )));
          }
        }
      }
      let patt = LinearCommand::LinearMap(patt_map);
      Ok(patt)
    }
    LuaValue::Integer(v1) => match exarg {
      Some(v2) => {
        let patt = LinearCommand::Linear(v1 as u32, v2);
        Ok(patt)
      }
      None => Err(LuaError::RuntimeError(format!(
        "In simple form, 2 parameters must be passed."
      ))),
    },
    _ => Err(LuaError::FromLuaConversionError {
      from: cmd.type_name(),
      to: "integer|table",
      message: None,
    }),
  }
}

fn lua_rotate_to_rust(cmd: LuaValue, exarg: Option<bool>) -> LuaResult<RotateCommand> {
  match cmd {
    LuaValue::Table(t) => {
      let mut patt_map: HashMap<u32, (f64, bool)> = HashMap::new();
      for kv in t.pairs::<LuaInteger, LuaNumber>() {
        let (k, mut v) = kv?;
        let cw = v < 0.0;
        if cw {
          v = -v;
        }
        match k.try_into() {
          Ok(krl) => {
            patt_map.insert(krl, (v, cw));
          }
          Err(err) => {
            return Err(LuaError::MemoryError(format!(
              "Device index out of range! {}",
              err
            )));
          }
        }
      }
      let patt = RotateCommand::RotateMap(patt_map);
      Ok(patt)
    }
    LuaValue::Number(v) => {
      let mut cw = v < 0.0;
      match exarg {
        Some(excw) => {
          if excw {
            cw = !cw;
          }
        }
        None => {}
      }
      let patt = RotateCommand::Rotate(v, cw);
      Ok(patt)
    }
    LuaValue::Integer(vi) => {
      let v = vi as f64;
      let mut cw = v < 0.0;
      match exarg {
        Some(excw) => {
          if excw {
            cw = !cw;
          }
        }
        None => {}
      }
      let patt = RotateCommand::Rotate(v, cw);
      Ok(patt)
    }
    _ => Err(LuaError::FromLuaConversionError {
      from: cmd.type_name(),
      to: "number|table",
      message: None,
    }),
  }
}

fn lua_endpoint_to_buttplug(ep: &String) -> LuaResult<Endpoint> {
  match ep.as_str() {
    "Command" => Ok(Endpoint::Command),
    "Firmware" => Ok(Endpoint::Firmware),
    "Rx" => Ok(Endpoint::Rx),
    "RxAccel" => Ok(Endpoint::RxAccel),
    "RxBLEBattery" => Ok(Endpoint::RxBLEBattery),
    "RxBLEModel" => Ok(Endpoint::RxBLEModel),
    "RxPressure" => Ok(Endpoint::RxPressure),
    "RxTouch" => Ok(Endpoint::RxTouch),
    "Tx" => Ok(Endpoint::Tx),
    "TxMode" => Ok(Endpoint::TxMode),
    "TxShock" => Ok(Endpoint::TxShock),
    "TxVibrate" => Ok(Endpoint::TxVibrate),
    "TxVendorControl" => Ok(Endpoint::TxVendorControl),
    "Whitelist" => Ok(Endpoint::Whitelist),
    "Generic0" => Ok(Endpoint::Generic0),
    "Generic1" => Ok(Endpoint::Generic1),
    "Generic2" => Ok(Endpoint::Generic2),
    "Generic3" => Ok(Endpoint::Generic3),
    "Generic4" => Ok(Endpoint::Generic4),
    "Generic5" => Ok(Endpoint::Generic5),
    "Generic6" => Ok(Endpoint::Generic6),
    "Generic7" => Ok(Endpoint::Generic7),
    "Generic8" => Ok(Endpoint::Generic8),
    "Generic9" => Ok(Endpoint::Generic9),
    "Generic10" => Ok(Endpoint::Generic10),
    "Generic11" => Ok(Endpoint::Generic11),
    "Generic12" => Ok(Endpoint::Generic12),
    "Generic13" => Ok(Endpoint::Generic13),
    "Generic14" => Ok(Endpoint::Generic14),
    "Generic15" => Ok(Endpoint::Generic15),
    "Generic16" => Ok(Endpoint::Generic16),
    "Generic17" => Ok(Endpoint::Generic17),
    "Generic18" => Ok(Endpoint::Generic18),
    "Generic19" => Ok(Endpoint::Generic19),
    "Generic20" => Ok(Endpoint::Generic20),
    "Generic21" => Ok(Endpoint::Generic21),
    "Generic22" => Ok(Endpoint::Generic22),
    "Generic23" => Ok(Endpoint::Generic23),
    "Generic24" => Ok(Endpoint::Generic24),
    "Generic25" => Ok(Endpoint::Generic25),
    "Generic26" => Ok(Endpoint::Generic26),
    "Generic27" => Ok(Endpoint::Generic27),
    "Generic28" => Ok(Endpoint::Generic28),
    "Generic29" => Ok(Endpoint::Generic29),
    "Generic30" => Ok(Endpoint::Generic30),
    "Generic31" => Ok(Endpoint::Generic31),
    _ => Err(LuaError::RuntimeError(format!(
      "Invalid argument. Cannot match endpoint string `{}`!",
      ep
    ))),
  }
}

#[derive(Clone)]
struct ClientDeviceUserData {
  _obj: Arc<ButtplugClientDevice>,
  _parent: Arc<ButtplugClient>,
  _rt: Arc<Runtime>,
}

impl ClientDeviceUserData {
  fn new(device: Arc<ButtplugClientDevice>, client: Arc<ButtplugClient>, rt: Arc<Runtime>) -> Self {
    ClientDeviceUserData {
      _obj: device,
      _parent: client,
      _rt: rt,
    }
  }
}

impl LuaUserData for ClientDeviceUserData {
  fn add_fields<'lua, F: LuaUserDataFields<'lua, Self>>(fields: &mut F) {
    fields.add_field_method_get("name", |_, dev| {
      let dev = &dev._obj;
      Ok(dev.name.clone())
    });
    fields.add_field_method_get("allowed_messages", |lua, dev| {
      let handle = &dev._obj;
      let caps = &handle.allowed_messages;
      let t = lua.create_table()?;
      for (msg, attrs) in caps {
        match msg {
          ButtplugClientDeviceMessageType::StopDeviceCmd => {
            t.set("stop_device", lua.to_value(&attrs)?)?;
          }
          ButtplugClientDeviceMessageType::VibrateCmd => {
            t.set("vibrate", lua.to_value(&attrs)?)?;
          }
          ButtplugClientDeviceMessageType::LinearCmd => {
            t.set("linear", lua.to_value(&attrs)?)?;
          }
          ButtplugClientDeviceMessageType::RotateCmd => {
            t.set("rotate", lua.to_value(&attrs)?)?;
          }
          ButtplugClientDeviceMessageType::BatteryLevelCmd => {
            t.set("battery_level", lua.to_value(&attrs)?)?;
          }
          ButtplugClientDeviceMessageType::RSSILevelCmd => {
            t.set("rssi_level", lua.to_value(&attrs)?)?;
          }
          ButtplugClientDeviceMessageType::RawReadCmd => {
            t.set("raw_read", lua.to_value(&attrs)?)?;
          }
          ButtplugClientDeviceMessageType::RawWriteCmd => {
            t.set("raw_write", lua.to_value(&attrs)?)?;
          }
          ButtplugClientDeviceMessageType::RawSubscribeCmd => {
            t.set("raw_subscribe", lua.to_value(&attrs)?)?;
          }
          ButtplugClientDeviceMessageType::RawUnsubscribeCmd => {
            t.set("raw_unsubscribe", lua.to_value(&attrs)?)?;
          }
        }
      }
      Ok(t)
    });
    fields.add_field_method_get("index", |_, dev| {
      let dev = &dev._obj;
      Ok(dev.index())
    });
    fields.add_field_method_get("connected", |_, dev| {
      let dev = &dev._obj;
      Ok(dev.connected())
    });
  }

  fn add_methods<'lua, M: LuaUserDataMethods<'lua, Self>>(methods: &mut M) {
    methods.add_async_method("vibrate", |_, cud, cmd: LuaValue| async move {
      let dev = &cud._obj;
      let rt = &cud._rt;
      // Convert lua table to vibrate obj.
      let patt = lua_vibrate_to_rust(cmd)?;
      // Perform transaction.
      {
        let _reactor = rt.enter();
        if let Err(error) = dev.vibrate(patt).await {
          Err(LuaError::RuntimeError(
            buttplug_client_error_to_lua_message(error),
          ))
        } else {
          Ok(())
        }
      }
    });
    methods.add_async_method(
      "linear",
      |_, cud, (cmd, excmd): (LuaValue, Option<LuaNumber>)| async move {
        let dev = &cud._obj;
        let rt = &cud._rt;
        // Convert lua table to vibrate obj.
        let patt = lua_linear_to_rust(cmd, excmd)?;
        // Perform transaction.
        {
          let _reactor = rt.enter();
          if let Err(error) = dev.linear(patt).await {
            Err(LuaError::RuntimeError(
              buttplug_client_error_to_lua_message(error),
            ))
          } else {
            Ok(())
          }
        }
      },
    );
    methods.add_async_method(
      "rotate",
      |_, cud, (cmd, excmd): (LuaValue, Option<bool>)| async move {
        let dev = &cud._obj;
        let rt = &cud._rt;
        // Convert lua table to vibrate obj.
        let patt = lua_rotate_to_rust(cmd, excmd)?;
        // Perform transaction.
        {
          let _reactor = rt.enter();
          if let Err(error) = dev.rotate(patt).await {
            Err(LuaError::RuntimeError(
              buttplug_client_error_to_lua_message(error),
            ))
          } else {
            Ok(())
          }
        }
      },
    );
    methods.add_async_method("battery_level", |lua, cud, ()| async move {
      let dev = &cud._obj;
      let rt = &cud._rt;
      // Perform transaction.
      {
        let _reactor = rt.enter();
        let status = dev.battery_level().await;
        match status {
          Err(error) => (mlua::Nil, buttplug_client_error_to_lua_message(error)).to_lua_multi(lua),
          Ok(v) => v.to_lua_multi(lua),
        }
      }
    });
    methods.add_async_method("rssi_level", |lua, cud, ()| async move {
      let dev = &cud._obj;
      let rt = &cud._rt;
      // Perform transaction.
      {
        let _reactor = rt.enter();
        let status = dev.rssi_level().await;
        match status {
          Err(error) => (mlua::Nil, buttplug_client_error_to_lua_message(error)).to_lua_multi(lua),
          Ok(v) => v.to_lua_multi(lua),
        }
      }
    });
    methods.add_async_method(
      "raw_write",
      |lua, cud, (ep_str, dat_str, write_with_response): (String, LuaString, bool)| async move {
        let dev = &cud._obj;
        let rt = &cud._rt;
        // Convert arguments.
        let ep = lua_endpoint_to_buttplug(&ep_str)?;
        let dat = dat_str.as_bytes().to_vec();
        // Perform transaction.
        {
          let _reactor = rt.enter();
          let status = dev.raw_write(ep, dat, write_with_response).await;
          match status {
            Err(error) => {
              (mlua::Nil, buttplug_client_error_to_lua_message(error)).to_lua_multi(lua)
            }
            Ok(_) => (true).to_lua_multi(lua),
          }
        }
      },
    );
    methods.add_async_method(
      "raw_read",
      |lua, cud, (ep_str, expected_length, timeout): (String, u32, u32)| async move {
        let dev = &cud._obj;
        let rt = &cud._rt;
        // Convert arguments.
        let ep = lua_endpoint_to_buttplug(&ep_str)?;
        // Perform transaction.
        {
          let _reactor = rt.enter();
          let status = dev.raw_read(ep, expected_length, timeout).await;
          match status {
            Err(error) => {
              (mlua::Nil, buttplug_client_error_to_lua_message(error)).to_lua_multi(lua)
            }
            Ok(v) => {
              // TODO: Test what type this actually converts into.
              (v).to_lua_multi(lua)
            }
          }
        }
      },
    );
    methods.add_async_method("raw_subscribe", |lua, cud, ep_str: String| async move {
      let dev = &cud._obj;
      let rt = &cud._rt;
      // Convert arguments.
      let ep = lua_endpoint_to_buttplug(&ep_str)?;
      // Perform transaction.
      {
        let _reactor = rt.enter();
        if let Err(error) = dev.raw_subscribe(ep).await {
          (mlua::Nil, buttplug_client_error_to_lua_message(error)).to_lua_multi(lua)
        } else {
          (true).to_lua_multi(lua)
        }
      }
    });
    methods.add_async_method("raw_unsubscribe", |lua, cud, ep_str: String| async move {
      let dev = &cud._obj;
      let rt = &cud._rt;
      // Convert arguments.
      let ep = lua_endpoint_to_buttplug(&ep_str)?;
      // Perform transaction.
      {
        let _reactor = rt.enter();
        if let Err(error) = dev.raw_unsubscribe(ep).await {
          (mlua::Nil, buttplug_client_error_to_lua_message(error)).to_lua_multi(lua)
        } else {
          (true).to_lua_multi(lua)
        }
      }
    });
    methods.add_async_method("stop", |lua, cud, ()| async move {
      let dev = &cud._obj;
      let rt = &cud._rt;
      // Perform transaction.
      {
        let _reactor = rt.enter();
        if let Err(error) = dev.stop().await {
          (mlua::Nil, buttplug_client_error_to_lua_message(error)).to_lua_multi(lua)
        } else {
          (true).to_lua_multi(lua)
        }
      }
    });
  }
}

#[derive(Clone)]
struct ClientUserData {
  _obj: Arc<ButtplugClient>,
  _rt: Arc<Runtime>,
}

impl ClientUserData {
  fn new(obj: ButtplugClient, rt: Runtime) -> Self {
    ClientUserData {
      _obj: Arc::new(obj),
      _rt: Arc::new(rt),
    }
  }
}

impl LuaUserData for ClientUserData {
  fn add_fields<'lua, F: LuaUserDataFields<'lua, Self>>(fields: &mut F) {
    fields.add_field_method_get("connected", |_, cud| {
      let client = &cud._obj;
      Ok(client.connected())
    });
    fields.add_field_method_get("server_name", |_, cud| {
      let client = &cud._obj;
      Ok(client.server_name())
    });
  }

  fn add_methods<'lua, M: LuaUserDataMethods<'lua, Self>>(methods: &mut M) {
    methods.add_meta_method(LuaMetaMethod::Close, |_, cud, _err: LuaValue| {
      let client = &cud._obj;
      let rt = &cud._rt;
      if client.connected() {
        if let Err(error) = rt.block_on(client.disconnect()) {
          Err(LuaError::RuntimeError(
            buttplug_client_error_to_lua_message(error),
          ))
        } else {
          Ok(())
        }
      } else {
        Ok(())
      }
    });

    methods.add_async_method("next_event", |lua, cud, ()| async move {
      // Retreive fields.
      let client = &cud._obj;
      // ?? Error `cannot unpin` when using Arc<dyn Stream> field.
      let mut events = client.event_stream();
      let rt = &cud._rt;
      // Prepare return value.
      let t = lua.create_table()?;
      {
        let _reactor = rt.enter();
        if let Some(event) = events.next().await {
          match event {
            ButtplugClientEvent::DeviceAdded(dev) => {
              t.set(
                "device_added",
                ClientDeviceUserData::new(dev.clone(), cud._obj.clone(), cud._rt.clone()),
              )?;
              Ok(t)
            }
            ButtplugClientEvent::DeviceRemoved(dev) => {
              t.set(
                "device_removed",
                ClientDeviceUserData::new(dev.clone(), cud._obj.clone(), cud._rt.clone()),
              )?;
              Ok(t)
            }
            ButtplugClientEvent::ScanningFinished => {
              t.set("scanning_finished", true)?;
              Ok(t)
            }
            ButtplugClientEvent::PingTimeout => {
              t.set("error_ping_timeout", true)?;
              Ok(t)
            }
            ButtplugClientEvent::ServerConnect => {
              t.set("error_server_connect", true)?;
              Ok(t)
            }
            ButtplugClientEvent::ServerDisconnect => {
              t.set("error_server_disconnect", true)?;
              Ok(t)
            }
            ButtplugClientEvent::Error(err) => {
              t.set("error_server_generic", buttplug_error_to_lua_message(err))?;
              Ok(t)
            }
          }
        } else {
          // Returns empty event structure.
          Ok(t)
        }
      }
    });
    methods.add_async_method("start_scanning", |lua, cud, ()| async move {
      let client = &cud._obj;
      let rt = &cud._rt;
      {
        let _reactor = rt.enter();
        if let Err(error) = client.start_scanning().await {
          (mlua::Nil, buttplug_client_error_to_lua_message(error)).to_lua_multi(lua)
        } else {
          (true).to_lua_multi(lua)
        }
      }
    });
    methods.add_async_method("stop_scanning", |lua, cud, ()| async move {
      let client = &cud._obj;
      let rt = &cud._rt;
      {
        let _reactor = rt.enter();
        if let Err(error) = client.stop_scanning().await {
          (mlua::Nil, buttplug_client_error_to_lua_message(error)).to_lua_multi(lua)
        } else {
          (true).to_lua_multi(lua)
        }
      }
    });
    methods.add_async_method("stop_all_devices", |lua, cud, ()| async move {
      let client = &cud._obj;
      let rt = &cud._rt;
      {
        let _reactor = rt.enter();
        if let Err(error) = client.stop_all_devices().await {
          (mlua::Nil, buttplug_client_error_to_lua_message(error)).to_lua_multi(lua)
        } else {
          (true).to_lua_multi(lua)
        }
      }
    });
    methods.add_method("get_devices", |lua, cud, ()| {
      let client = &cud._obj;
      let dev_vec = client.devices();
      match dev_vec.len().try_into() {
        Ok(dev_len) => {
          let t = lua.create_table_with_capacity(dev_len, 0)?;
          for (i, dev) in dev_vec.iter().enumerate() {
            t.set(
              i + 1,
              ClientDeviceUserData::new(dev.clone(), cud._obj.clone(), cud._rt.clone()),
            )?;
          }
          Ok(t)
        }
        Err(err) => Err(LuaError::MemoryError(err.to_string())),
      }
    });
    methods.add_async_method("ping", |lua, cud, ()| async move {
      let client = &cud._obj;
      let rt = &cud._rt;
      {
        let _reactor = rt.enter();
        if let Err(error) = client.ping().await {
          (mlua::Nil, buttplug_client_error_to_lua_message(error)).to_lua_multi(lua)
        } else {
          (true).to_lua_multi(lua)
        }
      }
    });
    methods.add_async_method("disconnect", |lua, cud, ()| async move {
      let client = &cud._obj;
      let rt = &cud._rt;
      {
        let _reactor = rt.enter();
        if let Err(error) = client.disconnect().await {
          (mlua::Nil, buttplug_client_error_to_lua_message(error)).to_lua_multi(lua)
        } else {
          (true).to_lua_multi(lua)
        }
      }
    });
  }
}

#[mlua::lua_module]
fn libbuttplug_lua(lua: &Lua) -> LuaResult<LuaTable> {
  let api = lua.create_table()?;
  let client_api = lua.create_table()?;
  let server_api = lua.create_table()?;

  // Experimental/Debug/Hidden API
  api.set(
    "enable_logging",
    lua.create_function(|_lua, ()| {
      tracing_subscriber::fmt::init();
      Ok(())
    })?,
  )?;

  // Public API.
  client_api.set(
    "new_insecure",
    lua.create_async_function(|lua, (name, address): (LuaString, LuaString)| async move {
      let connector = ButtplugRemoteClientConnector::<
        ButtplugWebsocketClientTransport,
        ButtplugClientJSONSerializer,
      >::new(ButtplugWebsocketClientTransport::new_insecure_connector(
        address.to_str()?,
      ));
      // Create a runtime and acquire a guard, as is required for async I/O.
      let rt = Runtime::new().unwrap();
      {
        let _reactor = rt.enter();
        let client = ButtplugClient::new(name.to_str()?);
        if let Err(error) = client.connect(connector).await {
          (mlua::Nil, buttplug_client_error_to_lua_message(error)).to_lua_multi(lua)
        } else {
          ClientUserData::new(client, rt).to_lua_multi(lua)
        }
      }
    })?,
  )?;

  client_api.set(
    "new_secure",
    lua.create_async_function(
      |lua, (name, address, bypass_cert_verify): (LuaString, LuaString, Option<bool>)| async move {
        let connector = ButtplugRemoteClientConnector::<
          ButtplugWebsocketClientTransport,
          ButtplugClientJSONSerializer,
        >::new(ButtplugWebsocketClientTransport::new_secure_connector(
          address.to_str()?,
          bypass_cert_verify.unwrap_or(false),
        ));
        // Create a runtime and acquire a guard, as is required for async I/O.
        let rt = Runtime::new().unwrap();
        {
          let _reactor = rt.enter();
          let client = ButtplugClient::new(name.to_str()?);
          if let Err(error) = client.connect(connector).await {
            (mlua::Nil, buttplug_client_error_to_lua_message(error)).to_lua_multi(lua)
          } else {
            ClientUserData::new(client, rt).to_lua_multi(lua)
          }
        }
      },
    )?,
  )?;

  client_api.set(
    "new_embedded",
    lua.create_async_function(|lua, name: LuaString| async move {
      let connector = ButtplugInProcessClientConnector::default();
      // Create a runtime and acquire a guard, as is required for async I/O.
      let rt = Runtime::new().unwrap();
      {
        let _reactor = rt.enter();
        let client = ButtplugClient::new(name.to_str()?);
        if let Err(error) = client.connect(connector).await {
          (mlua::Nil, buttplug_client_error_to_lua_message(error)).to_lua_multi(lua)
        } else {
          ClientUserData::new(client, rt).to_lua_multi(lua)
        }
      }
    })?,
  )?;

  api.set("client", client_api)?;
  api.set("server", server_api)?;

  Ok(api)
}
