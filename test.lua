local buttplug = require("libbuttplug_lua")

local function test_func()
    local o = buttplug.connect("FromLua Tester", "ws://127.0.0.1:12345", false)
    print("Connected:", o)
    o:stop_all_devices()
    print("Connected?:", o:is_connected())
    o:disconnect()
    print("Connected?:", o:is_connected())
end

thread = coroutine.wrap(test_func)

local i = 1
while thread() do
    print("Yields:", i)
    i = i + 1
end
