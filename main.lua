local buttplug = require("libbuttplug_lua")


local server_state = {handle = nil, tasks = {}, config = {
    address = "ws://127.0.0.1:12345",
    -- internal/local/secure
    mode = "local",
    -- Use the 'global' StopAllDevices command,
    -- instead of optionally disabling individual devices.
    use_stop_all = false
}}
local device_state = {}
local last_player_state = {playing = false, time = 0.0}
local global_time = 0.0

local disconnect_pending = false

local function device_register(btp_dev)
    o = {handle = btp_dev, enabled = false,
         actors = {}, task = nil, config = {
            update_rate = 100
        }}
    table.insert(device_state, o)
    return o, #device_state
end

local function device_unregister(obj_or_index)
    -- Index to be removed.
    index = nil
    -- Convert argument to a valid index.
    if type(obj_or_index) == "number" then
        index = obj_or_index
    else
        for i, o in ipairs(device_state) do
            if obj_or_index == o then
                index = i
                break
            end
        end
    end
    if index then
        table.remove(device_state, index)
    else
        print("device_unregister("..tostring(obj_or_index)..") not found!")
    end
end

local function device_set_enabled(obj, state)
end

function init()
    print("Buttplug extension for OFS loading with ".._VERSION..".")
    print("Buttplug extension for OFS loaded!")
end

function update(delta)
    global_time += delta
    -- Run tasks.
    -- Update player state.
end

function gui()
    -- Buttplug central.
    -- Device control list.
end
